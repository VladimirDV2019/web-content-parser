CREATE TABLE IF NOT EXISTS "user_role"
(
    id                         SERIAL PRIMARY KEY,
    role                       VARCHAR(1024) NOT NULL,
    user_id                    BIGINT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES "user"(id)
);