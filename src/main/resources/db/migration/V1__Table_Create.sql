CREATE TABLE IF NOT EXISTS statistic
(
    id                  SERIAL PRIMARY KEY,
    data                VARCHAR(50) NOT NULL,
    timestamp           BIGINT UNIQUE NOT NULL,
    content_url         VARCHAR(100)UNIQUE NOT NULL,
    content             VARCHAR(5000) NOT NULL,
    amount_same_words   INT NOT NULL,
    most_popular_word   VARCHAR(50) NOT NULL,
    amount_unique_words INT NOT NULL
);
