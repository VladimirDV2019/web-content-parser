CREATE TABLE IF NOT EXISTS "user"
(
    id                          SERIAL PRIMARY KEY,
    email                       VARCHAR(1024) NOT NULL UNIQUE,
    password                    VARCHAR(256) NOT NULL,
    login                       VARCHAR(1024) NOT NULL UNIQUE
);