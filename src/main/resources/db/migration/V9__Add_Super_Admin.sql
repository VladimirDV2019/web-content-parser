INSERT INTO "user" (email,password,login)
VALUES ('tezzrere@gmail.com','$2a$10$m34oO.QxeXcawlVZ4v0vhuALtTp.kkXVr/h6CYSMTFgu9vrI8yZRq','DRei');

with super_admin_id as (select currval('user_id_seq'))
insert into user_role(role, user_id)
values ('SUPER_ADMIN', (select * from super_admin_id));
