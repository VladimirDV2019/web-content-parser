CREATE TABLE IF NOT EXISTS errors
(
    id                  SERIAL PRIMARY KEY,
    data                VARCHAR(10) NOT NULL,
    timestamp           BIGINT UNIQUE NOT NULL,
    content_url         VARCHAR(100) NOT NULL,
    exception_name      VARCHAR(100) NOT NULL,
    exception_text      VARCHAR(100) NOT NULL,
    exception_cause     VARCHAR(100)
);
