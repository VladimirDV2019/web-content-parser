package com.dudkin.internal;

import com.dudkin.exceptions.ParserNotFoundException;
import com.dudkin.exceptions.UrlException;
import com.dudkin.internal.parsers.TextFinder;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
class SuitableParserSearcher {
    private List<TextFinder> allTextFinders;

    public SuitableParserSearcher(List<TextFinder> allTextFinders) {
        this.allTextFinders = allTextFinders;
    }

    public TextFinder getSuitableTextFinder(String url) throws IOException {
        try {
            Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new UrlException("Something get wrong with url", e);
        }
        for (TextFinder textFinder : allTextFinders) {
            if (textFinder.checkForSuitability(url))
                return textFinder;
        }
        throw new ParserNotFoundException("This application doesn't work with this page!");
    }
}
