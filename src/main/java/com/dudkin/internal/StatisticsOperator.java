package com.dudkin.internal;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Component
public class StatisticsOperator {
    public void setInBase(List<String> urls, ConfigurableApplicationContext context) {
        ThreadPoolTaskExecutor pool = context.getBean(ThreadPoolTaskExecutor.class);
        List<Future<String>> futures = new ArrayList<>();
        for (String url : urls) {
            SetInBaseThread setInBaseThread = context.getBean(SetInBaseThread.class);
            setInBaseThread.setUrl(url);
            futures.add(pool.submit(setInBaseThread));
        }
        for (Future<String> future : futures) {
            while (!future.isDone()) {
            }
        }
        pool.shutdown();
    }
}
