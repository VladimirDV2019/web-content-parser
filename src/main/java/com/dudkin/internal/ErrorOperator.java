package com.dudkin.internal;

import com.dudkin.dao.ErrorDao;
import com.dudkin.dao.ErrorEntity;
import com.dudkin.dao.UserDao;
import com.dudkin.web.TokenDecoder;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
class ErrorOperator {
    private ErrorDao errorDao;
    private UserDao userDao;
    private TokenDecoder tokenDecoder;

    public ErrorOperator(ErrorDao errorDao, UserDao userDao, TokenDecoder tokenDecoder) {
        this.errorDao = errorDao;
        this.userDao = userDao;
        this.tokenDecoder = tokenDecoder;
    }

    public void setInBase(String name, String text, String url, String cause) {
        ErrorEntity errorEntity = new ErrorEntity();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        errorEntity.setData(simpleDateFormat.format(new Date()));
        errorEntity.setTimestamp(System.currentTimeMillis());
        errorEntity.setContentUrl(url);
        errorEntity.setExceptionName(name);
        errorEntity.setExceptionText(text);
        errorEntity.setExceptionCause(cause);
        errorEntity.setUser(tokenDecoder.getUser());
        errorDao.create(errorEntity);
    }
}
