package com.dudkin.internal;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
class ContentOperator {
    private FileOperator fileOperator;
    private FilePathMaker filePathMaker;
    private static final Integer indentFromLanguage = 1;
    private static final Integer shiftDueToInformationSplit = 2;

    public ContentOperator(FileOperator fileOperator, FilePathMaker filePathMaker) {
        this.fileOperator = fileOperator;
        this.filePathMaker = filePathMaker;
    }

    public String getContent(Integer contentIndex, String url) throws IOException {
        return getAllContent(url).get(contentIndex * shiftDueToInformationSplit + indentFromLanguage);
    }

    public String getLanguage(Integer contentIndex, String url) throws IOException {
        return getAllContent(url).get(contentIndex * shiftDueToInformationSplit);
    }

    public Integer getLength(String url) throws IOException {
        return (getAllContent(url).size() / 2);
    }

    private List<String> getAllContent(String url) throws IOException {
        File file = new File(filePathMaker.getPath(url));
        if (!file.exists()) {
            fileOperator.writeToFile(url);
        }
        List<String> contentList = fileOperator.readFromFile(url);
        return contentList;
    }
}

