package com.dudkin.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class UnsortedStatisticCreator {

    public Map<String, Integer> getUnsortedStatistic(String text) {
        Map<String, Integer> unsortedStatistic = new HashMap<String, Integer>();
        for (String str : getWords(text)) {
            if (unsortedStatistic.containsKey(str))
                unsortedStatistic.put(str, unsortedStatistic.get(str) + 1);
            else
                unsortedStatistic.put(str, 1);
        }
        return unsortedStatistic;
    }

    private List<String> getWords(String text) {
        String[] r = text.split("\\s|,|\\?|\\[|\\]|\\.|\\!|\"|:|;");
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < r.length; i++) {
            if ((!(r[i].length() < 1) && !(r[i].matches("\\d|-")))) {
                list.add(r[i].toLowerCase());
            }
        }
        return list;
    }
}
