package com.dudkin.internal;

import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
class FilePathMaker {
    private SuitableParserSearcher searcher;

    public FilePathMaker(SuitableParserSearcher searcher) {
        this.searcher = searcher;
    }

    public String getPath(String url) throws IOException {
        String[] strings = searcher.getSuitableTextFinder(url).getText(url).split("\n");
        return "./src/main/resources/texts/" + strings[1] + ".txt";
    }
}
