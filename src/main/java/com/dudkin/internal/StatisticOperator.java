package com.dudkin.internal;

import com.dudkin.dao.StatisticDao;
import com.dudkin.dao.StatisticEntity;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class StatisticOperator {
    private ContentOperator contentOperator;
    private StatisticDao statisticDao;
    private StatisticCreator statisticCreator;
    private ErrorOperator errorOperator;

    public StatisticOperator(ContentOperator contentOperator,
                             StatisticDao statisticDao,
                             StatisticCreator statisticCreator,
                             ErrorOperator errorOperator) {
        this.contentOperator = contentOperator;
        this.statisticDao = statisticDao;
        this.statisticCreator = statisticCreator;
        this.errorOperator = errorOperator;
    }

    public void setInBase(String url) {
        try {
            for (int i = 0; i < contentOperator.getLength(url); i++) {
                StatisticEntity statisticEntity = new StatisticEntity();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                statisticEntity.setData(simpleDateFormat.format(new Date()));
                statisticEntity.setTimestamp(System.currentTimeMillis());
                statisticEntity.setContentUrl(url);
                statisticEntity.setContent(contentOperator.getContent(i, url));
                statisticEntity.setAmountWords(Integer.valueOf(statisticCreator.getStatisticElement(i, 0, url)));
                statisticEntity.setAmountSameWords(Integer.valueOf(statisticCreator.getStatisticElement(i, 1, url)));
                statisticEntity.setMostPopularWord(statisticCreator.getStatisticElement(i, 2, url));
                statisticEntity.setAmountUniqueWords(Integer.valueOf(statisticCreator.getStatisticElement(i, 3, url)));
                statisticEntity.setLanguage(contentOperator.getLanguage(i, url));
                statisticDao.create(statisticEntity);
            }
        } catch (Exception e) {
            String cause;
            String[] str = e.toString().split(":");
            try {
                cause = e.getCause().toString();
            } catch (NullPointerException e1) {
                cause = null;
            }
            errorOperator.setInBase(str[0], e.getMessage(), url, cause);
            e.printStackTrace();
        }
    }

    public StatisticDao getByIdOrByUrlOrGetAll() {
        return statisticDao;
    }
}
