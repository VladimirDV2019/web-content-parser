package com.dudkin.internal.creation.parts;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;
@Component
@Order(1)
public class WordsCounter implements StatisticElement {
    @Override
    public String findStatisticElement(Map<String, Integer> unsortedStatistic) {
        return Integer.toString(unsortedStatistic.size());
    }
}
