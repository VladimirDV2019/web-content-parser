package com.dudkin.internal.creation.parts;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Order(3)
public class MostPopularWordFinder implements StatisticElement {

    @Override
    public String findStatisticElement(Map<String, Integer> unsortedStatistic) {
        Integer maxValue = 1;
        String popularWord = null;
        for (Map.Entry<String, Integer> entry : unsortedStatistic.entrySet()) {
            if (entry.getValue() > maxValue) {
                maxValue = entry.getValue();
                popularWord = entry.getKey();
            }
        }
        return popularWord;
    }
}
