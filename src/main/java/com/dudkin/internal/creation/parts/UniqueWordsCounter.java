package com.dudkin.internal.creation.parts;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;
@Component
@Order(4)
public class UniqueWordsCounter implements StatisticElement {
    @Override
    public String findStatisticElement(Map<String, Integer> unsortedStatistic) {
        Integer count = 0;
        for (Map.Entry<String, Integer> entry : unsortedStatistic.entrySet()) {
            if (entry.getValue()==1)
                count++;
        }
        return count.toString();
    }
}
