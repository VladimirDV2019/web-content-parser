package com.dudkin.internal.creation.parts;

import java.util.Map;

public interface StatisticElement {
    String findStatisticElement(Map<String, Integer> unsortedStatistic);
}
