package com.dudkin.internal;

import com.dudkin.internal.creation.parts.StatisticElement;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
class StatisticCreator {
    private ContentOperator contentOperator;
    private List<StatisticElement> elements;

    public StatisticCreator(ContentOperator contentOperator, List<StatisticElement> elements) {
        this.contentOperator = contentOperator;
        this.elements = elements;
    }

    private Map<String, Integer> getUnsortedStatistic(Integer contentIndex, String url) throws IOException {
        UnsortedStatisticCreator unsortedStatistic = new UnsortedStatisticCreator();
        return unsortedStatistic.getUnsortedStatistic(contentOperator.getContent(contentIndex, url));
    }

    public String getStatisticElement(Integer contentIndex, Integer statisticElementIndex, String url) throws IOException {
        return elements.get(statisticElementIndex).findStatisticElement(getUnsortedStatistic(contentIndex, url));
    }

}