package com.dudkin.internal.parsers;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SecondTextFinder implements TextFinder {

    @Override
    public String getText(String url) throws IOException {
        String result = "";
        Elements elements = Jsoup.connect(url).get().select("p");
        if (elements.get(0).childNode(1).toString().contains("br"))
            result = firstСase(elements);
        else
            result = secondCase(elements);
        return result;
    }

    private String firstСase(Elements elements) {
        String result = "LANGUAGE-Russian\n";
        for (int i = 0; i < elements.get(0).childNodeSize(); i++) {
            String line = elements.get(0).childNode(i).toString();
            if (!line.contains("Припев")
                    && !line.contains("br")
                    && !line.contains("span")
                    && !line.contains("Куплет")
                    && !line.contains("Источник")
                    && !line.contains("Слова по англиски:")
                    && !line.matches(" "))
                result = result + line + "\n";
            else if (line.contains("Слова по англиски:")) {
                result += "\nLANGUAGE-English\n";
            }
        }
        return result;
    }

    private String secondCase(Elements elements) {
        String result = "LANGUAGE-English\n";
        for (int j = 0; j < 2; j++) {
            if (j == 1)
                result += "\nLANGUAGE-Russian\n";
            for (int i = 0; i < elements.get(j).childNodeSize(); i++) {
                if (elements.get(j).childNode(i).childNodeSize() != 0) {
                    String line = elements.get(j).childNode(i).childNode(0).toString();
                    if (!line.contains("br")
                            && !line.contains("Источник")
                            && !line.matches(" "))
                        result = result + line + "\n";
                }
            }
        }
        return result;
    }

    @Override
    public Boolean checkForSuitability(String url) {
        return url.contains("https://teksty-pesenok.ru");
    }
}
