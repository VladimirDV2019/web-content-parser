package com.dudkin.internal.parsers;

import java.io.IOException;


public interface TextFinder {
    String getText(String url) throws IOException;

    Boolean checkForSuitability(String url);
}
