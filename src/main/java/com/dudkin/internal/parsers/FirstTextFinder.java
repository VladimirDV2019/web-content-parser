package com.dudkin.internal.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class FirstTextFinder implements TextFinder {

    @Override
    public String getText(String url) throws IOException {
        Element element = Jsoup.connect(url).get().selectFirst("p");
        String result = "LANGUAGE-Russian\n";
        for (int i = 0; i < element.childNodeSize(); i++) {
            String line = element.childNode(i).toString();
            if (!line.contains("Припев")
                    && !line.contains("br")
                    && !line.contains("Куплет")
                    && !line.matches(" "))
                result = result + line + "\n";
        }
        return result;
    }

    @Override
    public Boolean checkForSuitability(String url) {
        return url.contains("https://altwall.net");
    }
}