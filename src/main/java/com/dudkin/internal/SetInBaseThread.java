package com.dudkin.internal;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;

@Component
@Setter
@Scope("prototype")
class SetInBaseThread implements Callable {
    private String url;
    @Autowired
    private StatisticOperator statisticOperator;

    @Override
    public Object call() {
        statisticOperator.setInBase(url);
        return null;
    }
}
