package com.dudkin.internal;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
class FileOperator {
    private FilePathMaker filePathMaker;
    private SuitableParserSearcher searcher;

    public FileOperator(FilePathMaker filePathMaker, SuitableParserSearcher searcher) {
        this.filePathMaker = filePathMaker;
        this.searcher = searcher;
    }

    public List<String> readFromFile(String url) throws IOException {
        List<String> list = new ArrayList<>();
        String text;
        BufferedReader reader = new BufferedReader(new FileReader(filePathMaker.getPath(url)));
        text = "";
        String temp;
        Boolean alreadyWas = false;
        while ((temp = reader.readLine()) != null) {
            if ((alreadyWas == true) && (temp.contains("LANGUAGE"))) {
                list.add(text);
                text = "";
                String[] anotherTemp = temp.split("-");
                list.add(anotherTemp[1]);
            } else if (temp.contains("LANGUAGE")) {
                String[] anotherTemp = temp.split("-");
                list.add(anotherTemp[1]);
                alreadyWas = true;
            } else
                text = text + temp + "\n";
        }
        list.add(text);
        reader.close();
        return list;
    }

    public void writeToFile(String url) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePathMaker.getPath(url)));
        writer.write(searcher.getSuitableTextFinder(url).getText(url));
        writer.close();
    }
}
