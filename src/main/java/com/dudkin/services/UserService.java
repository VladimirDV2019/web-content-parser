package com.dudkin.services;

import com.dudkin.dao.UserDao;
import com.dudkin.dao.UserEntity;
import com.dudkin.dao.UserRoleEntity;
import com.dudkin.enums.UserRole;
import com.dudkin.exceptions.ResourceNotFoundException;
import com.dudkin.web.dto.CreateUserDto;
import com.dudkin.web.dto.UserDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;


    }

    public UserDto saveUser(CreateUserDto user) {
        UserRoleEntity userRoleEntity = new UserRoleEntity();
        UserEntity userEntity = transformDtoToEntity(user);
        userRoleEntity.setRole(UserRole.USER);
        userRoleEntity.setUserEntity(userEntity);
        userEntity.setUserRoles(Collections.singletonList(userRoleEntity));
        return transformEntityToDto(userDao.save(userEntity));
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userDao.findOneByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User with email:" + email + " not found"));
        List<GrantedAuthority> authorities = userEntity.getUserRoles()
                .stream()
                .map(this::toAuthority)
                .collect(Collectors.toList());
        return new User(userEntity.getEmail(), userEntity.getPassword(), authorities);
    }

    private GrantedAuthority toAuthority(UserRoleEntity userRoleEntity) {
        return new SimpleGrantedAuthority(userRoleEntity.getRole().name());
    }

    private UserEntity transformDtoToEntity(CreateUserDto user) {
        UserEntity entity = new UserEntity();
        entity.setEmail(user.getEmail());
        entity.setLogin(user.getLogin());
        entity.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return entity;
    }

    UserDto transformEntityToDto(UserEntity userEntity) {
        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());
        userDto.setLogin(userEntity.getLogin());
        userDto.setEmail(userEntity.getEmail());
        return userDto;
    }
}
