package com.dudkin.services;

import com.dudkin.dao.StatisticEntity;
import com.dudkin.exceptions.UrlException;
import com.dudkin.internal.StatisticOperator;
import com.dudkin.internal.StatisticsOperator;
import com.dudkin.web.TokenDecoder;
import com.dudkin.web.dto.CalculateStatisticsDto;
import com.dudkin.web.dto.StatisticDto;
import com.dudkin.web.dto.StatisticListDto;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParserService {
    private StatisticOperator statisticOperator;
    private TokenDecoder tokenDecoder;

    public ParserService(StatisticOperator statisticOperator, TokenDecoder tokenDecoder) {
        this.statisticOperator = statisticOperator;
        this.tokenDecoder = tokenDecoder;
    }

    public StatisticEntity getStatisticById(Long id) {
        return statisticOperator.getByIdOrByUrlOrGetAll().getById(id);
    }

    public List<StatisticEntity> getStatisticByUrl(String url) {
        return statisticOperator.getByIdOrByUrlOrGetAll().getByUrl(url);
    }

    public StatisticListDto calculateStatistics(CalculateStatisticsDto dto,
                                                StatisticsOperator statisticsOperator,
                                                ConfigurableApplicationContext applicationContext) throws UrlException {
        tokenDecoder.setUser();
        List<String> allUrls = dto.getUrls();
        List<String> newUrls = clearKnownUrls(allUrls);
        statisticsOperator.setInBase(newUrls, applicationContext);
        List<StatisticEntity> foundEntities = new ArrayList<>();
        int urlNum = 1;
        for (String url : allUrls) {
            if (getStatisticByUrl(url).isEmpty())
                throw new UrlException("Check " + urlNum + " url");
            foundEntities.addAll(getStatisticByUrl(url));
            urlNum++;
        }
        return transformEntitiesToListDto(foundEntities);
    }

    public List<String> clearKnownUrls(List<String> allUrls) {
        List<String> newUrls = new ArrayList<>();
        for (String url : allUrls) {
            if (getStatisticByUrl(url).isEmpty()) {
                newUrls.add(url);
            }
        }
        return newUrls;
    }

    public StatisticListDto transformEntitiesToListDto(List<StatisticEntity> foundEntities) {
        StatisticListDto statisticListDto = new StatisticListDto();
        statisticListDto.setUrlsCount(foundEntities.size());
        List<StatisticDto> dtoList = new ArrayList<>();
        for (StatisticEntity entity : foundEntities) {
            if (entity != null)
                dtoList.add(transformEntityToDto(entity));
        }
        statisticListDto.setContent(dtoList);
        return statisticListDto;
    }

    public StatisticDto transformEntityToDto(StatisticEntity entity) {
        StatisticDto statisticDto = new StatisticDto();
        statisticDto.setAmountSameWords(entity.getAmountSameWords());
        statisticDto.setAmountUniqueWords(entity.getAmountUniqueWords());
        statisticDto.setAmountWords(entity.getAmountWords());
        statisticDto.setContent(entity.getContent());
        statisticDto.setContentUrl(entity.getContentUrl());
        statisticDto.setLanguage(entity.getLanguage());
        statisticDto.setMostPopularWord(entity.getMostPopularWord());
        return statisticDto;
    }

}
