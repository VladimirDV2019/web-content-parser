package com.dudkin.services;

import com.dudkin.dao.UserDao;
import com.dudkin.dao.UserEntity;
import com.dudkin.enums.UserRole;
import com.dudkin.exceptions.ResourceNotFoundException;
import com.dudkin.web.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService {
    private UserDao userDao;
    private UserService userService;

    public UsersService(UserDao userDao, UserService userService) {
        this.userDao = userDao;
        this.userService = userService;
    }

    public UserDto getUserById(Long userId) {
        return userService.transformEntityToDto(userDao.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("Can't find user with id " + userId)));
    }

    public List<UserDto> getAllUsers() {
        List<UserDto> usersDtos = new ArrayList<>();
        List<UserEntity> usersEntities = userDao.findAll();
        for (UserEntity userEntity : usersEntities) {
            if (!userEntity.getUserRoles().get(0).getRole().equals(UserRole.SUPER_ADMIN))
                usersDtos.add(userService.transformEntityToDto(userEntity));
        }
        return usersDtos;
    }
}
