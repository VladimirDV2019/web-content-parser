package com.dudkin.services;

import com.dudkin.dao.ErrorDao;
import com.dudkin.dao.ErrorEntity;
import com.dudkin.dao.UserDao;
import com.dudkin.dao.UserEntity;
import com.dudkin.exceptions.DaoException;
import com.dudkin.web.dto.ErrorDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ErrorsService {
    private ErrorDao errorDao;
    private UserService userService;
    private UserDao userDao;

    public ErrorsService(ErrorDao errorDao, UserService userService, UserDao userDao) {
        this.errorDao = errorDao;
        this.userService = userService;
        this.userDao = userDao;
    }

    public List<ErrorDto> getAllErrors() {
        List<ErrorEntity> errorEntities = errorDao.getAll();
        return transformEntityListToDtoList(errorEntities);
    }

    public List<ErrorDto> getErrorsByUserId(Long userId) {
        UserEntity userEntity = userDao.findById(userId)
                .orElseThrow(() -> new DaoException("Can`t find user with id " + userId));
        List<ErrorEntity> errorEntities = errorDao.getByUser(userEntity);
        return transformEntityListToDtoList(errorEntities);
    }

    private List<ErrorDto> transformEntityListToDtoList(List<ErrorEntity> errorEntities) {
        List<ErrorDto> errorDtoList = new ArrayList<>();
        errorEntities.forEach(errorEntity -> errorDtoList.add(transformEntityToDto(errorEntity)));
        return errorDtoList;
    }

    ErrorDto transformEntityToDto(ErrorEntity errorEntity) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setId(errorEntity.getId());
        errorDto.setContentUrl(errorEntity.getContentUrl());
        errorDto.setData(errorEntity.getData());
        errorDto.setExceptionCause(errorEntity.getExceptionCause());
        errorDto.setExceptionName(errorEntity.getExceptionName());
        errorDto.setExceptionText(errorEntity.getExceptionText());
        errorDto.setTimestamp(errorEntity.getTimestamp());
        errorDto.setUserDto(userService.transformEntityToDto(errorEntity.getUser()));
        return errorDto;
    }
}
