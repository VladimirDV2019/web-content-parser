package com.dudkin.models;

import com.dudkin.enums.UserRole;

public class Role {
    private UserRole role;

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
