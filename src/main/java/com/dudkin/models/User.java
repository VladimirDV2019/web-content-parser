package com.dudkin.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class User {
    private String login;
    private String email;
    private List<Role> roles;
}
