package com.dudkin.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "statistic")
public class StatisticEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "data")
    private String data;
    @Column(name = "timestamp")
    private Long timestamp;
    @Column(name = "content_url")
    private String contentUrl;
    @Column(name = "content")
    private String content;
    @Column(name = "amount_words")
    private Integer amountWords;
    @Column(name = "amount_same_words")
    private Integer amountSameWords;
    @Column(name = "most_popular_word")
    private String mostPopularWord;
    @Column(name = "amount_unique_words")
    private Integer amountUniqueWords;
    @Column(name = "language")
    private String language;
}
