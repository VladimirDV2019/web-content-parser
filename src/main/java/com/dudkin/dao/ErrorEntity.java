package com.dudkin.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "errors")
public class ErrorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "data")
    private String data;
    @Column(name = "timestamp")
    private Long timestamp;
    @Column(name = "content_url")
    private String contentUrl;
    @Column(name = "exception_name")
    private String exceptionName;
    @Column(name = "exception_text")
    private String exceptionText;
    @Column(name = "exception_cause")
    private String exceptionCause;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;
}
