package com.dudkin.dao;

import com.dudkin.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "user_role")
public class UserRoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private UserRole role;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;
}
