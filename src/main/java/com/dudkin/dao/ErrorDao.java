package com.dudkin.dao;

import com.dudkin.exceptions.DaoException;
import com.dudkin.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.util.List;

@Component
public class ErrorDao {
    private SessionFactory sessionFactory;

    public ErrorDao(EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
    }

    public ErrorEntity getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            ErrorEntity foundEntity = session.get(ErrorEntity.class, id);
            if (foundEntity == null)
                throw new EntityNotFoundException("Can't fetch error with id: " + id + " from database");
            return foundEntity;
        } catch (Exception e) {
            throw new DaoException("Can't fetch error with id: " + id + " from database", e);
        }
    }

    public List<ErrorEntity> getByUser(UserEntity userEntity) {
        try (Session session = sessionFactory.openSession()) {
            Query<ErrorEntity> query = session.createQuery("SELECT a FROM ErrorEntity a WHERE a.user = :user",
                    ErrorEntity.class);
            query.setParameter("user", userEntity);
            List<ErrorEntity> foundEntity;
            try {
                foundEntity = query.getResultList();
            } catch (NoResultException e) {
                return null;
            }
            return foundEntity;
        } catch (Exception e) {
            throw new DaoException("Can't find errors of user with id : " + userEntity.getId() + " from database", e);
        }
    }

    public ErrorEntity create(ErrorEntity entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Long storedEntityId = (Long) session.save(entity);
                session.getTransaction().commit();
                entity.setId(storedEntityId);
                return entity;
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new DaoException("Can't store error to database", e);
            }
        }
    }

    public List<ErrorEntity> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("SELECT a FROM ErrorEntity a", ErrorEntity.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Can't get any errors from database", e);
        }
    }
}
