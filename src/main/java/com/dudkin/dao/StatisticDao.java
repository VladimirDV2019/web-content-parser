package com.dudkin.dao;

import com.dudkin.exceptions.DaoException;
import com.dudkin.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.util.List;

@Component
public class StatisticDao {
    private SessionFactory sessionFactory;

    public StatisticDao(EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
    }

    public StatisticEntity getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            StatisticEntity foundEntity = session.get(StatisticEntity.class, id);
            if (foundEntity == null)
                throw new EntityNotFoundException("Can't fetch statistic with id: " + id + " from database");
            return foundEntity;
        } catch (Exception e) {
            throw new DaoException("Can't fetch statistic with id: " + id + " from database", e);
        }
    }

    public List<StatisticEntity> getByUrl(String contentUrl) {
        try (Session session = sessionFactory.openSession()) {
            Query<StatisticEntity> query = session.createQuery("SELECT a FROM StatisticEntity a WHERE a.contentUrl = :contentUrl",
                    StatisticEntity.class);
            query.setParameter("contentUrl", contentUrl);
            List<StatisticEntity> foundEntity;
            try {
                foundEntity = query.getResultList();
            } catch (NoResultException e) {
                return null;
            }
            return foundEntity;
        } catch (Exception e) {
            throw new DaoException("Can't fetch statistic with url: " + contentUrl + " from database", e);
        }
    }

    public StatisticEntity create(StatisticEntity entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Long storedEntityId = (Long) session.save(entity);
                session.getTransaction().commit();
                entity.setId(storedEntityId);
                return entity;
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new DaoException("Can't store statistic to database", e);
            }
        }
    }

    public List<StatisticEntity> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("SELECT a FROM StatisticEntity a", StatisticEntity.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Can't get any statistic from database", e);
        }
    }
}
