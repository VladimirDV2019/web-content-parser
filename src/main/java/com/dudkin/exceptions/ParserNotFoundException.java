package com.dudkin.exceptions;

import java.io.IOException;

public class ParserNotFoundException extends IOException {
    public ParserNotFoundException(String message) {
        super(message);
    }
}
