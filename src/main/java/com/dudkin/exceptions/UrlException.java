package com.dudkin.exceptions;

import java.io.IOException;

public class UrlException extends IOException {
    public UrlException(String message, Throwable cause) {
        super(message, cause);
    }

    public UrlException(String message) {
        super(message);
    }
}
