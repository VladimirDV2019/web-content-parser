package com.dudkin.web.handler;

import com.dudkin.exceptions.DaoException;
import com.dudkin.exceptions.UrlException;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class SimpleExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DaoException.class)
    protected ResponseEntity<SimpleException> handleDaoException(Exception e) {
        return new ResponseEntity<>(new SimpleException(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UrlException.class)
    protected ResponseEntity<SimpleException> handleUrlException(Exception e) {
        return new ResponseEntity<>(new SimpleException(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Data
    private static class SimpleException {
        private String message;

        public SimpleException(String message) {
            this.message = message;
        }
    }
}