package com.dudkin.web;

import com.dudkin.dao.UserDao;
import com.dudkin.dao.UserEntity;
import com.dudkin.exceptions.DaoException;
import com.dudkin.models.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class TokenDecoder {
    private UserDao userDao;
    private User user;

    public TokenDecoder(UserDao userDao) {
        this.userDao = userDao;

    }

    public void setUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = (User) auth.getPrincipal();
    }

    public UserEntity getUser() {
        return userDao.findOneByEmail(user.getEmail()).orElseThrow(
                () -> new DaoException("Can`t find user with email" + user.getEmail()));
    }
}
