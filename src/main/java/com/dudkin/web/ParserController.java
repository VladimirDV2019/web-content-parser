package com.dudkin.web;

import com.dudkin.exceptions.DaoException;
import com.dudkin.exceptions.UrlException;
import com.dudkin.internal.StatisticsOperator;
import com.dudkin.services.ParserService;
import com.dudkin.web.dto.CalculateStatisticsDto;
import com.dudkin.web.dto.StatisticDto;
import com.dudkin.web.dto.StatisticListDto;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/statistics")
public class ParserController {
    private StatisticsOperator statisticsOperator;
    private ConfigurableApplicationContext applicationContext;
    private ParserService parserService;

    public ParserController(StatisticsOperator statisticsOperator,
                            ConfigurableApplicationContext applicationContext,
                            ParserService parserService) {
        this.statisticsOperator = statisticsOperator;
        this.applicationContext = applicationContext;
        this.parserService = parserService;
    }

    @PutMapping
    @ResponseBody
    public StatisticListDto calculateStatistics(@RequestBody CalculateStatisticsDto dto) throws UrlException {
        return parserService.calculateStatistics(dto, statisticsOperator, applicationContext);
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    public StatisticDto showStatistic(@PathVariable("id") Long id) throws DaoException {
        StatisticDto statisticDto;
        try {
            statisticDto = parserService.transformEntityToDto(parserService.getStatisticById(id));
        } catch (Exception e) {
            throw new DaoException("Can't find statistic with id " + id);
        }
        return statisticDto;
    }
}
