package com.dudkin.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticDto {
    @JsonProperty("content_url")
    private String contentUrl;
    private String content;
    @JsonProperty("amount_words")
    private Integer amountWords;
    @JsonProperty("amount_same_words")
    private Integer amountSameWords;
    @JsonProperty("most_popular_word")
    private String mostPopularWord;
    @JsonProperty("amount_unique_words")
    private Integer amountUniqueWords;
    private String language;
}
