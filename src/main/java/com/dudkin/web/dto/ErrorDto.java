package com.dudkin.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDto {
    private Long id;
    private String data;
    private Long timestamp;
    private String contentUrl;
    private String exceptionName;
    private String exceptionText;
    private String exceptionCause;
    private UserDto userDto;
}
