package com.dudkin.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class StatisticListDto {
    @JsonProperty("urls_count")
    private Integer urlsCount;
    private List<StatisticDto> content;

    public Integer getUrlsCount() {
        return urlsCount;
    }

    public void setUrlsCount(Integer urlsCount) {
        this.urlsCount = urlsCount;
    }

    public List<StatisticDto> getContent() {
        return content;
    }

    public void setContent(List<StatisticDto> content) {
        this.content = content;
    }
}
