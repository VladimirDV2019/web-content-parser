package com.dudkin.web.dto;

import java.util.List;

public class CalculateStatisticsDto {
    private List<String> urls;

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }
}
