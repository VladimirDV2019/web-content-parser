package com.dudkin.web;

import com.dudkin.exceptions.DaoException;
import com.dudkin.services.ErrorsService;
import com.dudkin.web.dto.ErrorDto;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@PreAuthorize("hasRole('ROLE_SUPER_ADMIN') || hasRole('ROLE_ADMIN')")
@RequestMapping(value = "/errors")
public class ErrorsController {
    private ErrorsService errorsService;

    public ErrorsController(ErrorsService errorsService) {
        this.errorsService = errorsService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ErrorDto> getAllErrors() {
        return errorsService.getAllErrors();
    }

    @GetMapping(path = "/{userId}")
    @ResponseBody
    public List<ErrorDto> getErrorsByUserId(@PathVariable("userId") Long userId) throws DaoException {
        return errorsService.getErrorsByUserId(userId);
    }
}
