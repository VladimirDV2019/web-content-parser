package com.dudkin.web;

import com.dudkin.exceptions.DaoException;
import com.dudkin.services.UsersService;
import com.dudkin.web.dto.UserDto;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
@RequestMapping(value = "/users")
public class UsersController {
    private UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserDto> getAllUsers() {
        return usersService.getAllUsers();
    }

    @GetMapping(path = "/{userId}")
    @ResponseBody
    public UserDto getUserById(@PathVariable("userId") Long userId) throws DaoException {
        return usersService.getUserById(userId);
    }
}
