package com.dudkin.internal.creation.parts;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class SameWordsCounterTest {
    private StatisticElement statisticElement = new SameWordsCounter();

    @Test
    public void sameWordsCountWillBeFound() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        unsortedStatistic.put("Bird", 2);
        unsortedStatistic.put("cucumber", 1);
        unsortedStatistic.put("auto-closable", 4);
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "2");
    }

    @Test
    public void ZeroWillBeReturnedWhenUnsortedStatisticIsNull() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "0");
    }
}