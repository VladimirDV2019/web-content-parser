package com.dudkin.internal.creation.parts;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class WordsCounterTest {
    private StatisticElement statisticElement = new WordsCounter();

    @Test
    public void wordsWillBeCounted() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        unsortedStatistic.put("pain", 34);
        unsortedStatistic.put("shadow", 5);
        unsortedStatistic.put("blood", 9);
        unsortedStatistic.put("revenge", 1);
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "4");
    }

    @Test
    public void ZeroWillBeReturnedWhenUnsortedStatisticIsNull() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "0");
    }
}