package com.dudkin.internal.creation.parts;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class MostPopularWordFinderTest {
    private StatisticElement statisticElement = new MostPopularWordFinder();

    @Test
    public void mostPopularWordWillBeFound() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        unsortedStatistic.put("my", 4);
        unsortedStatistic.put("balloon", 22);
        unsortedStatistic.put("furrer", 1);
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "balloon");
    }

    @Test
    public void NullWillBeReturnedWhenUnsortedStatisticIsNull() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertNull(statisticElement);
    }

}