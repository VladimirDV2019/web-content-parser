package com.dudkin.internal.creation.parts;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class UniqueWordsCounterTest {
    private StatisticElement statisticElement = new UniqueWordsCounter();

    @Test
    public void sameWordsCountWillBeFound() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        unsortedStatistic.put("plane", 1);
        unsortedStatistic.put("fit", 3);
        unsortedStatistic.put("rock", 4);
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "1");
    }

    @Test
    public void ZeroWillBeReturnedWhenUnsortedStatisticIsNull() {
        Map<String, Integer> unsortedStatistic = new HashMap<>();
        String statisticElement = this.statisticElement.findStatisticElement(unsortedStatistic);
        Assertions.assertEquals(statisticElement, "0");
    }

}